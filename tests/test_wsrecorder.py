__author__ = 'kamil'
import os
import os.path
import tempfile
import shutil
from copy import deepcopy

from unittest2 import TestCase
from lxml import etree
from lxml.etree import tostring as xml2str, fromstring as str2xml, _Element
from xpath2_functions import register_functions
from wsrecorder import WSRecorder, ConfigurationException, NotRecordedException
from wsrecorder.tools import lxml_compare


def web_service_mockup(request):
    if isinstance(request, _Element):
        return xml2str(request)
    else:
        return request


MEAN_ATTR = 'XYZ&quot;\'&%\\'


msg_param1_x_lxml = str2xml('<request><msg1><param1>X</param1></msg1></request>')
msg_param1_y_lxml = str2xml('<request><msg1><param1>Y</param1></msg1></request>')
msg_param2_y_lxml = str2xml('<request><msg1><param2>Y</param2></msg1></request>')
msg_param3_x_lxml = str2xml('<request><msg1><param3>X</param3></msg1></request>')
msg_param3_y_lxml = str2xml('<request><msg1><param3>Y</param3></msg1></request>')
msg_param3_z_lxml = str2xml('<request><msg1><param3>Z</param3></msg1></request>')
msg_param1_x_param2_x_lxml = str2xml('<request><msg1><param1>X</param1><param2>X</param2></msg1></request>')
msg_param1_z_param2_x_lxml = str2xml('<request><msg1><param1>Z</param1><param2>X</param2></msg1></request>')
msg_param1_x_another_param2_x_param3_a_lxml = str2xml('''<request><msg>
    <param1>X</param1><another><param2>X</param2><param3>A</param3></another></msg></request>''')
msg_param1_x_another_param2_y_param3_b_lxml = str2xml('''<request><msg>
    <param1>X</param1><another><param2>Y</param2><param3>B</param3></another></msg></request>''')
msg_param1_x_another_param2_y_param3_a_lxml = str2xml('''<request><msg>
    <param1>X</param1><another><param2>Y</param2><param3>A</param3></another></msg></request>''')
msg_param1_x_another_param2_z_param3_b_lxml = str2xml('''<request><msg>
    <param1>X</param1><another><param2>Z</param2><param3>B</param3></another></msg></request>''')
msg_param1_x_another_param2_z_param3_a_lxml = str2xml('''<request><msg>
    <param1>X</param1><another><param2>Z</param2><param3>A</param3></another></msg></request>''')
msg_param1_y_another_param2_z_param3_b_lxml = str2xml('''<request><msg>
    <param1>Y</param1><another><param2>Z</param2><param3>B</param3></another></msg></request>''')


class WsRecorder(TestCase):

    def assertLxmlEqual(self, xml1, xml2, msg=None):
        reporter = []
        output = lxml_compare(xml1, xml2, reporter.append)
        if not output:
            diffMsg = '\n'.join(reporter)
            msg = self._formatMessage(msg, diffMsg)
            self.fail(msg)

    def __init__(self, *args, **kwargs):
        super(TestCase, self).__init__(*args, **kwargs)
        self._type_equality_funcs[etree._Element] = self.assertLxmlEqual

    @classmethod
    def setUpClass(cls):
        register_functions(etree)
        cls.tmpdir = tempfile.mkdtemp()

    @classmethod
    def init_recorders(cls, name):
        output_dir = os.path.join(cls.tmpdir, name)
        os.makedirs(output_dir)
        params = {
            'request_msg_name': '//request/*',
            'reply_envelope_body': '//request/*',
            'messages': {
                'msg1': [
                    '//msg1/param1/text()',
                    '//msg1/param2/text()'
                ],
                'msg2': [
                    '//msg2/param1[@x="%s"]/text()' % MEAN_ATTR
                ],
                'msg-no-file': [],
            },
            'output_dir': output_dir,
            'pretty_print': True,
        }

        record = WSRecorder(mode=WSRecorder.Mode.RECORD, **params)
        override = WSRecorder(mode=WSRecorder.Mode.OVERRIDE, **params)
        serve = WSRecorder(mode=WSRecorder.Mode.SERVE, **params)
        transparent = WSRecorder(mode=WSRecorder.Mode.TRANSPARENT, **params)
        serve_transparent = WSRecorder(mode=WSRecorder.Mode.SERVE_TRANSPARENT, **params)
        serve_record = WSRecorder(mode=WSRecorder.Mode.SERVE_RECORD, **params)
        params_conflicted = deepcopy(params)
        params_conflicted['messages']['msg1'] = list(reversed(params_conflicted['messages']['msg1']))
        record_conflicted = WSRecorder(mode=WSRecorder.Mode.RECORD, **params_conflicted)

        return {
            'record': record.decorator(web_service_mockup),
            'override': override.decorator(web_service_mockup),
            'serve': serve.decorator(web_service_mockup),
            'transparent': transparent.decorator(web_service_mockup),
            'serve_transparent': serve_transparent.decorator(web_service_mockup),
            'serve_record': serve_record.decorator(web_service_mockup),
            'record_conflicted': record_conflicted.decorator(web_service_mockup)
        }

    def test_exceptions(self):
        output_dir = os.path.join(self.tmpdir, 'test_exceptions')
        os.makedirs(output_dir)
        params = {
            'request_msg_name': '//request/*',
            'reply_envelope_body': '//request/*',
            'messages': {
                'msg1': [
                    '//msg1/param1/text()',
                    '//msg1/param2/text()'
                ],
            },
            'mode': WSRecorder.Mode.SERVE_RECORD,
            'output_dir': output_dir
        }

        test_params = deepcopy(params)
        test_params['output_dir'] = os.path.join(WsRecorder.tmpdir, 'non_existing_dir')
        self.assertRaisesRegexp(
            ConfigurationException, u"Given output directory path: .* is not proper",
            WSRecorder, **test_params
        )

        test_params = deepcopy(params)
        test_params['request_msg_name'] = '...//request/*'
        self.assertRaisesRegexp(
            ConfigurationException, u"Request message name is not proper",
            WSRecorder, **test_params
        )

        test_params = deepcopy(params)
        test_params['reply_envelope_body'] = '....//request/*'
        self.assertRaisesRegexp(
            ConfigurationException, u"Reply envelope body is not proper xpath",
            WSRecorder, **test_params
        )

        test_params = deepcopy(params)
        test_params['messages'] = {'msg1': ['.../request']}
        self.assertRaisesRegexp(
            ConfigurationException, u"Path .* for message .* is not proper xpath",
            WSRecorder, **test_params
        )

        test_params = deepcopy(params)
        test_params['mode'] = 'XYZ',
        self.assertRaisesRegexp(
            ConfigurationException, u"Chosen mode .* is not allowed",
            WSRecorder, **test_params
        )

        test_params = deepcopy(params)
        test_params['request_msg_name'] = '//request2/*'
        recorder = WSRecorder(**test_params)
        decorated = recorder.decorator(web_service_mockup)
        self.assertRaisesRegexp(
            ConfigurationException, u"XPath with request msg name doesn't return any node for given message.",
            decorated, msg_param1_x_lxml
        )

        test_params = deepcopy(params)
        test_params['reply_envelope_body'] = '//request2/*'
        recorder = WSRecorder(**test_params)
        decorated = recorder.decorator(web_service_mockup)
        self.assertRaisesRegexp(
            ConfigurationException, u"XPath with reply envelope body doesn't return any node for given message.",
            decorated, msg_param1_x_lxml
        )

    def test_basic(self):
        recorders = WsRecorder.init_recorders('test_basic')
        self.assertEqual(msg_param1_x_lxml, str2xml(recorders['record'](msg_param1_x_lxml)))
        self.assertEqual(msg_param1_y_lxml, str2xml(recorders['record'](msg_param1_y_lxml)))
        self.assertEqual(msg_param1_x_param2_x_lxml, str2xml(recorders['record'](msg_param1_x_param2_x_lxml)))
        self.assertEqual(msg_param1_z_param2_x_lxml, str2xml(recorders['record'](msg_param1_z_param2_x_lxml)))
        self.assertEqual(msg_param1_x_lxml, str2xml(recorders['serve'](msg_param1_x_lxml)))
        self.assertEqual(msg_param1_y_lxml, str2xml(recorders['serve'](msg_param1_y_lxml)))
        self.assertEqual(msg_param1_x_param2_x_lxml, str2xml(recorders['serve'](msg_param1_x_param2_x_lxml)))
        self.assertEqual(msg_param1_z_param2_x_lxml, str2xml(recorders['serve'](msg_param1_z_param2_x_lxml)))

    def test_not_override_mode(self):
        recorders = WsRecorder.init_recorders('test_not_override_mode')

        self.assertEqual(msg_param3_x_lxml, str2xml(recorders['record'](msg_param3_x_lxml)))
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['record'](msg_param3_y_lxml)))

        # reply for input1 i input2 are equal, because input2 didnt override the input1
        self.assertEqual(msg_param3_x_lxml, str2xml(recorders['serve'](msg_param3_x_lxml)))
        self.assertEqual(msg_param3_x_lxml, str2xml(recorders['serve'](msg_param3_y_lxml)))

    def test_not_sampled_msg(self):
        recorders = WsRecorder.init_recorders('test_not_sampled_msg')

        msg_notsample_param3_x = str2xml('<request><msg-notsample><param3>X</param3></msg-notsample></request>')
        msg_notsample_param3_y = str2xml('<request><msg-notsample><param3>Y</param3></msg-notsample></request>')

        self.assertEqual(msg_notsample_param3_x, str2xml(recorders['record'](msg_notsample_param3_x)))
        self.assertEqual(msg_notsample_param3_y, str2xml(recorders['record'](msg_notsample_param3_y)))
        self.assertEqual(msg_notsample_param3_x, str2xml(recorders['serve'](msg_notsample_param3_x)))
        self.assertEqual(msg_notsample_param3_y, str2xml(recorders['serve'](msg_notsample_param3_y)))

    def test_transparent_mode(self):
        recorders = WsRecorder.init_recorders('test_transparent_mode')

        # both request hit the same node in the tree (param1='', param2='')
        self.assertEqual(msg_param3_x_lxml, str2xml(recorders['record'](msg_param3_x_lxml)))
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['record'](msg_param3_y_lxml)))
        # Serve gets reply from recorded, so input2 reply is not overriding
        self.assertEqual(msg_param3_x_lxml, str2xml(recorders['serve'](msg_param3_x_lxml)))
        self.assertEqual(msg_param3_x_lxml, str2xml(recorders['serve'](msg_param3_y_lxml)))
        # Transparent hits the service, so input == output all the time
        self.assertEqual(msg_param3_x_lxml, str2xml(recorders['transparent'](msg_param3_x_lxml)))
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['transparent'](msg_param3_y_lxml)))

    def test_override_mode(self):
        recorders = WsRecorder.init_recorders('test_override_mode')

        self.assertEqual(msg_param3_x_lxml, str2xml(recorders['override'](msg_param3_x_lxml)))
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['override'](msg_param3_y_lxml)))
        # reply for input1 i input2 are equal, because input2 overrides the input1
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['serve'](msg_param3_x_lxml)))
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['serve'](msg_param3_y_lxml)))

    def test_recording_conflict(self):
        recorders = WsRecorder.init_recorders('test_recording_conflict')

        recorders['record'](msg_param2_y_lxml)
        recorders['record'](msg_param1_y_lxml)
        self.assertRaisesRegexp(
            ConfigurationException, u"There is a conflict between recorded sample vs current configuration.",
            recorders['record_conflicted'], msg_param2_y_lxml
        )

    def test_no_file(self):
        recorders = WsRecorder.init_recorders('test_no_file')
        self.assertRaises(NotRecordedException, recorders['serve'],
                          '<request><msg-no-file><param1>X</param1></msg-no-file></request>')

    def test_mean_xpath(self):
        recorders = WsRecorder.init_recorders('test_mean_xpath')
        msg2_param1_x_lxml = str2xml('<request><msg2><param1>X</param1></msg2></request>')
        msg2_param1_y_lxml = str2xml('<request><msg2><param1>Y</param1></msg2></request>')
        msg2_param1_x_lxml.xpath('//param1')[0].attrib['x'] = MEAN_ATTR
        msg2_param1_y_lxml.xpath('//param1')[0].attrib['x'] = MEAN_ATTR

        self.assertEqual(msg2_param1_x_lxml, str2xml(recorders['record'](msg2_param1_x_lxml)))
        self.assertEqual(msg2_param1_y_lxml, str2xml(recorders['record'](msg2_param1_y_lxml)))
        self.assertEqual(msg2_param1_x_lxml, str2xml(recorders['serve'](msg2_param1_x_lxml)))
        self.assertEqual(msg2_param1_y_lxml, str2xml(recorders['serve'](msg2_param1_y_lxml)))

    def test_notrecorded_reply_exception(self):
        recorders = WsRecorder.init_recorders('test_notrecorded_reply_exception')

        recorders['record'](msg_param1_x_lxml)
        self.assertRaises(NotRecordedException, recorders['serve'],
                          str2xml('<request><msg1><param1>NONE</param1></msg1></request>'))

    def test_serve_transparent(self):
        """
        # record: param1 = X
        # serve: param1 = Y -> Exception
        # serve_transparent param1 X = X
        # serve_transparent param1 Y = Ya
        """
        recorders = WsRecorder.init_recorders('test_serve_transparent')

        recorders['record'](msg_param1_x_lxml)
        self.assertRaises(NotRecordedException, recorders['serve'], msg_param1_y_lxml)

        self.assertEqual(msg_param1_x_lxml, str2xml(recorders['serve_transparent'](msg_param1_x_lxml)))
        self.assertEqual(msg_param1_y_lxml, str2xml(recorders['serve_transparent'](msg_param1_y_lxml)))

        self.assertRaises(NotRecordedException, recorders['serve'], msg_param1_y_lxml)

    def test_serve_record(self):
        """
        # record: param1 = X
        # serve: param1 = Y -> Exception
        # serve_record param1 X -> X
        # serve_record param1 Y
        # serve_record param3 Y
        # serve_record param3 Z
        # serve param1 Y -> Y
        # serve param3 Y -> Y
        # serve param3 Z -> Y
        """
        recorders = WsRecorder.init_recorders('test_serve_record')

        str2xml(recorders['serve_transparent'](msg_param1_x_lxml))
        self.assertRaises(NotRecordedException, recorders['serve'], msg_param1_y_lxml)

        self.assertEqual(msg_param1_x_lxml, str2xml(recorders['serve_record'](msg_param1_x_lxml)))
        self.assertEqual(msg_param1_y_lxml, str2xml(recorders['serve_record'](msg_param1_y_lxml)))
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['serve_record'](msg_param3_y_lxml)))
        #is already recorded, cant record again
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['serve_record'](msg_param3_z_lxml)))

        self.assertEqual(msg_param1_y_lxml, str2xml(recorders['serve'](msg_param1_y_lxml)))
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['serve'](msg_param3_y_lxml)))
        self.assertEqual(msg_param3_y_lxml, str2xml(recorders['serve'](msg_param3_z_lxml)))

    def test_multiple_xpath_values(self):
        output_dir = os.path.join(self.tmpdir, 'test_multiple_xpath_values')
        os.makedirs(output_dir)
        params = {
            'request_msg_name': '//request/*',
            'reply_envelope_body': '//request/*',
            'messages': {
                'msg': [
                    "xp2f:string-join(//msg/param1/text(), ',')",
                ],
            },
            'mode': WSRecorder.Mode.SERVE_RECORD,
            'output_dir': output_dir,
            'pretty_print': True,
        }

        msg = str2xml('<request><msg><param1>X</param1><param1>Y</param1><param1>Z</param1></msg></request>')
        msg2 = str2xml('<request><msg><param1>X</param1><param1>Z</param1></msg></request>')
        recorder = WSRecorder(**params)
        decorated = recorder.decorator(web_service_mockup)
        self.assertEqual(msg, str2xml(decorated(msg)))  # recording
        self.assertEqual(msg2, str2xml(decorated(msg2)))  # recording
        self.assertEqual(msg, str2xml(decorated(msg)))  # serving
        self.assertEqual(msg2, str2xml(decorated(msg2)))  # serving

    def test_another_reply_envelope_body(self):
        output_dir = os.path.join(self.tmpdir, 'test_another_reply_envelope_body')
        os.makedirs(output_dir)
        params = {
            'request_msg_name': '//request/*',
            'reply_envelope_body': '//request/*',
            'messages': {
                'msg': [
                    "//msg/param1/text()",
                    ("//msg/another/param2/text()",
                     "./another/param2")  # referencing to reply_envelope_body
                ],
            },
            'mode': WSRecorder.Mode.SERVE_RECORD,
            'output_dir': output_dir,
            'pretty_print': True,
        }

        msg1 = msg_param1_x_another_param2_x_param3_a_lxml
        msg2 = msg_param1_x_another_param2_y_param3_b_lxml
        msg3 = msg_param1_x_another_param2_z_param3_b_lxml
        msg4 = msg_param1_y_another_param2_z_param3_b_lxml

        msg2_param3_a = msg_param1_x_another_param2_y_param3_a_lxml
        msg3_param3_a = msg_param1_x_another_param2_z_param3_a_lxml

        recorder = WSRecorder(**params)
        decorated = recorder.decorator(web_service_mockup)
        self.assertEqual(msg1, str2xml(decorated(msg1)))  # recording
        self.assertEqual(msg2, str2xml(decorated(msg2)))  # recording
        self.assertEqual(msg3, str2xml(decorated(msg3)))  # recording
        self.assertEqual(msg4, str2xml(decorated(msg4)))  # recording
        self.assertEqual(msg1, str2xml(decorated(msg1)))  # serving
        self.assertEqual(msg2_param3_a, str2xml(decorated(msg2)))  # serving param3 = B?, because it is not overriden
        self.assertEqual(msg3_param3_a, str2xml(decorated(msg3)))  # serving param3 = B?, because it is not overriden
        self.assertEqual(msg4, str2xml(decorated(msg4)))  # serving

    def test_another_reply_envelope_body_two_bodies(self):
        output_dir = os.path.join(self.tmpdir, 'test_another_reply_envelope_body_two_bodies')
        os.makedirs(output_dir)
        params = {
            'request_msg_name': '//request/*',
            'reply_envelope_body': '//request/*',
            'messages': {
                'msg': [
                    "//msg/param1/text()",
                    ("//msg/another/param2/text()", "//msg/another/param2"),
                    ("//msg/another/param3/text()", "//msg/another/param3")
                ],
            },
            'mode': WSRecorder.Mode.SERVE_RECORD,
            'output_dir': output_dir
        }

        msg1 = msg_param1_x_another_param2_x_param3_a_lxml
        msg2 = msg_param1_x_another_param2_y_param3_b_lxml
        msg3 = msg_param1_x_another_param2_z_param3_b_lxml
        recorder = WSRecorder(**params)
        decorated = recorder.decorator(web_service_mockup)
        self.assertEqual(msg1, str2xml(decorated(msg1)))  # recording
        self.assertEqual(msg2, str2xml(decorated(msg2)))  # recording
        self.assertEqual(msg3, str2xml(decorated(msg3)))  # recording
        self.assertEqual(msg1, str2xml(decorated(msg1)))  # serving
        self.assertEqual(msg2, str2xml(decorated(msg2)))  # serving
        self.assertEqual(msg3, str2xml(decorated(msg3)))  # serving

    @classmethod
    def tearDownClass(cls):
        print cls.tmpdir
        #shutil.rmtree(cls.tmpdir)